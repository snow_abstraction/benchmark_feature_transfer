#!/usr/bin/env python3

# Copyright 2019 Douglas Wayne Potter
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Runs some unscientific benchmarks of other programs that transmit arrays over
TCP. Array sizes are fixed in the source code below. Minimal error checking.
"""

import json
import subprocess
import sys
import socket
import time

import pandas

PATH_PER_SENDERS = {
    "cpp_flatbuffers": "flat/flatbuffers/cpp_sender/build/release_bin/sender",
    "c_sprintf_json": "flat/json/c_sprintf_sender/build/release_bin/sender",
    "cpp_double_conversion_json": "flat/json/cpp_double_conversion_sender/build/release_bin/sender",
    "c_raw": "flat/raw/c_sender/build/release_bin/sender",
    "go_safe_raw": "flat/raw/go_safe_sender/bin/sender",
    "go_unsafe_raw": "flat/raw/go_unsafe_sender/bin/sender",
    "go_idiomatic_flatbuffers": "flat/flatbuffers/go_idiomatic_sender/bin/sender",
    "go_fast_flatbuffers": "flat/flatbuffers/go_fast_sender/bin/sender",
    "go_protobuf": "flat/protobuf/go_sender/bin/sender",
}

PATH_PER_RECEIVERS = {
    "python_flatbuffers": "flat/flatbuffers/py_receiver/receiver",
    "python_json": "flat/json/py_receiver/receiver",
    "python_raw": "flat/raw/py_receiver/receiver",
    "python_protobuf": "flat/protobuf/py_receiver/receiver",
}

SENDER_TYPE_AND_RECEIVER_TYPE_PER_BENCHMARK_NAME = {
    "cpp_python_flatbuffers": ("cpp_flatbuffers", "python_flatbuffers"),
    "go_python_idiomatic_flatbuffers": ("go_idiomatic_flatbuffers", "python_flatbuffers"),
    "go_python_fast_flatbuffers": ("go_fast_flatbuffers", "python_flatbuffers"),
    "c_sprintf_python_json": ("c_sprintf_json", "python_json"),
    "cpp_double_conversion_python_json": ("cpp_double_conversion_json", "python_json"),
    "c_python_raw": ("c_raw", "python_raw"),
    "safe_go_python_raw": ("go_safe_raw", "python_raw"),
    "unsafe_go_python_raw": ("go_unsafe_raw", "python_raw"),
    "go_python_protobuf": ("go_protobuf", "python_protobuf"),
}


def run_reciever_and_sender(sender, receiver, seconds_to_run, host, port):
    # Each sender message will contain num_samples * features_per_sample float32s
    num_samples = 1000
    features_per_sample = 100

    # receiver process is the server, it waits and listens for the sender to connect
    receiver_process = subprocess.Popen(
        # False = don't check that messages are correct
        # Running with True is important when testing the code.
        [receiver, host, port, "False"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    # hack: give the receiver a chance to listen on the socket
    time.sleep(1)
    sender_process = subprocess.Popen(
        [sender, str(num_samples), str(features_per_sample), host, port],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    try:
        (sender_out, sender_err) = sender_process.communicate(timeout=seconds_to_run)
    except subprocess.TimeoutExpired:
        sender_process.terminate()
        (sender_out, sender_err) = sender_process.communicate()
    (receiver_out, receiver_err) = receiver_process.communicate(1)

    assert receiver_process.poll() == 0
    assert sender_process.poll() == 0
    assert receiver_err == b""
    assert sender_err == b""
    return sender_out, receiver_out


def extract_json_receiver_results_from_output(receiver_output):
    decoded_output = receiver_output.decode("utf-8")
    results_line = decoded_output.split("Results: ")[-1].split("\n")[0]
    return pandas.read_json(results_line, orient="records", typ="series")


def run_benchmark(
    benchmark_name, sender_type, receiver_type, seconds_to_run, host, port
):
    sender = PATH_PER_SENDERS[sender_type]
    receiver = PATH_PER_RECEIVERS[receiver_type]

    _, receiver_output = run_reciever_and_sender(
        sender, receiver, seconds_to_run, host, port
    )
    results = extract_json_receiver_results_from_output(receiver_output)
    results["seconds_run"] = seconds_to_run
    results["sender_type"] = sender_type
    results["receiver_type"] = receiver_type
    results["benchmark"] = benchmark_name
    results.name = benchmark_name
    return results


def check_port_available(host, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        if s.connect_ex((host, port)) == 0:
            print("Port already in use.")
            exit(0)


def main():
    if len(sys.argv) < 5:
        print(
            "Usage: receiver <host ip> <port> <seconds to run benchmark> <results file>"
        )
        exit(0)

    host = sys.argv[1]
    port = sys.argv[2]
    seconds_to_run_benchmark = int(sys.argv[3])
    results_file = sys.argv[4]

    check_port_available(host, int(port))

    results = pandas.DataFrame()
    for (
        name,
        (sender_type, receiver_type),
    ) in SENDER_TYPE_AND_RECEIVER_TYPE_PER_BENCHMARK_NAME.items():
        print(
            f"running sender [{sender_type}] and receiver [{receiver_type}] for {seconds_to_run_benchmark} seconds"
        )
        sys.stdout.flush()
        result = run_benchmark(
            name, sender_type, receiver_type, seconds_to_run_benchmark, host, port
        )
        results = results.append(result)

    results.to_csv(results_file)


if __name__ == "__main__":
    main()
