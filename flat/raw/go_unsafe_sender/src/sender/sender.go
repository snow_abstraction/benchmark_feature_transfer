/*
Copyright 2019 Douglas Wayne Potter

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Disclaimer this is my first Go program. Just trying stuff out.

package main

import (
	"encoding/binary"
	"errors"
	"fmt"
	"log"
	"math"
	"net"
	"os"
	"os/signal"
	"reflect"
	"strconv"
	"syscall"
	"time"
	"unsafe"
)

type programArgs struct {
	nSamples int
	nFeatures int
	host string
	port string
}

func parseArgs() (programArgs, error) {
	args := programArgs{}

	if len(os.Args) != 5 {
		err := errors.New("Usage: send_features <number_of_samples> <number_of_features> <host> <port>")
		return args, err
	}

	nSamples, err := strconv.Atoi(os.Args[1])
	if err != nil {
		return args, err
	}
	if nSamples < 1 {
		err := errors.New("<number_of_samples> must be an integer greater than zero")
		return args, err
	}

	nFeatures, err := strconv.Atoi(os.Args[2])
	if err != nil {
		return args, err
	}
	if nFeatures < 1 {
		err := errors.New("<number_of_features> must be an integer greater than zero")
		return args, err
	}

	args.nSamples = nSamples
	args.nFeatures = nFeatures
	args.host = os.Args[3]
	args.port = os.Args[4]
	return args, nil
}

func makeBenchmarkData(n int) []float32 {
	// The receiver can construct the same sequence to verify that the data
	// was sent correctly i.e. we are benchmarking the same transmission of data
	data := make([]float32, n)
	for i := 0; i < n; i++ {
		data[i] = float32(math.Sin(float64(i)))
	}
	return data
}

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
}

func run() error {
	terminateChan := make(chan os.Signal, 1)
	signal.Notify(terminateChan, syscall.SIGINT, syscall.SIGTERM)

	args, err := parseArgs()
	if err != nil {
		return err
	}

	endPoint := args.host + ":" + args.port
	conn, err := net.Dial("tcp", endPoint)
	if err != nil {
		return err
	}
	defer conn.Close()

	fmt.Printf("Connected to %s.", endPoint)

	data := makeBenchmarkData(args.nSamples * args.nFeatures)

	keepRunning := true
	go func() {
		<-terminateChan
		keepRunning = false
	}()

	sendData := make([]float32, len(data))
	var nSent int64
	msgLen := make([]byte, 4)

	start := time.Now()
	for keepRunning {
		// benchmark should include cost/time copying
		// this is not optimized away when I profiled
		copy(sendData, data)


		// cast float32 to bytes
		// rely on the Go spec that float32 is IEEE-754 32-bit floating-point number
		// and the receiver also is LittleEndian
		const bytesPerFloat32 = 4 // bytes
		header := *(*reflect.SliceHeader)(unsafe.Pointer(&sendData))
		header.Len *= bytesPerFloat32
		header.Cap *= bytesPerFloat32
		sendBytes := *(*[]byte)(unsafe.Pointer(&header))


		binary.LittleEndian.PutUint32(msgLen, uint32(header.Len))

		_, err := conn.Write(msgLen)
		if err != nil {
			return err
		}
		_, err = conn.Write(sendBytes)
		if err != nil {
			return err
		}

		nSent++
	}

	stop := time.Now()
	elapsed := stop.Sub(start)
	nSentPerMs := float64(nSent) / float64(elapsed.Milliseconds())

	fmt.Print("Messages sent: ", nSent)
	fmt.Print("Messages sent per ms: ", nSentPerMs)

	return nil
}
