cmake_minimum_required(VERSION 3.13)

project(benchmark_feature_transformation)

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Debug)
endif()

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
  # error-limit is clang specific
  if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    add_compile_options(-g -O0 -ferror-limit=1)
  else()
    add_compile_options(-g -O0)
  endif()
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/debug_bin)
elseif(CMAKE_BUILD_TYPE STREQUAL "Release")
  add_compile_options(-O3 -mavx -DNDEBUG)
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/release_bin)
elseif(CMAKE_BUILD_TYPE STREQUAL "Sanitize")
  add_compile_options(-g -O1 -fno-omit-frame-pointer -fsanitize=address)
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/sanitize_bin)
else()
  message(FATAL_ERROR "Unknown type of build specified by CMAKE_BUILD_TYPE.")
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  add_compile_options(-ferror-limit=1)
endif()

add_compile_options(-Wall -Wpedantic -Werror)
set(CMAKE_C_STANDARD 99)

add_executable(sender
  src/main.c)

if(CMAKE_BUILD_TYPE STREQUAL "Sanitize")
  target_link_libraries(sender -fsanitize=address)
endif()

target_link_libraries(sender m)
