#!/usr/bin/env python3

# Copyright 2019 Douglas Wayne Potter
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json # just for formatting text output
import socket
import struct
import sys
import time

import numpy


class ZeroBytesRead(Exception):
    """Exception raised when socket connect return zero bytes when
       more than zero were excpeted.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message
        super().__init__()


class ArrayChecker:
    def __init__(self):
        self.expected_array = None

    def _set_expected_array(self, n):
        self.expected_array = numpy.sin(numpy.arange(0, n, dtype="float32"))

    def check(self, array):
        if self.expected_array is None:
            self._set_expected_array(len(array))

        numpy.testing.assert_array_equal(
            self.expected_array, array, verbose=True
        )


def read_array_messages(connection, check_messages):
    array_checker = ArrayChecker()
    number_of_messages = 0

    array = None
    try:
        start_time = time.time()
        while True:
            array = read_array(connection)
            if check_messages:
                array_checker.check(array)
            number_of_messages += 1
    except ZeroBytesRead:
        if number_of_messages < 1:
            raise
        # intrepret ZeroBytesRead exception as other side shutting down
        stop_time = time.time()
        elapsed_time = stop_time - start_time
        messages_per_ms = number_of_messages / elapsed_time / 1000
        ms_per_message = 1 / messages_per_ms
        us_per_message = 1000 * ms_per_message

        print("Results: {}".format(json.dumps({
            'messages_per_millisecond': messages_per_ms,
            'millseconds_per_message': ms_per_message,
            'microseconds_per_message': us_per_message
            })))


def main():
    if len(sys.argv) < 4:
        print("Usage: receiver <host> <port> <check_messages:True|False>")
        exit(0)

    print("Waiting for client...")

    host = str(sys.argv[1])
    port = int(sys.argv[2])
    check_messages_arg = sys.argv[3]
    assert check_messages_arg in [str(True), str(False)]
    check_messages = check_messages_arg == str(True)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as feature_socket:
        feature_socket.bind((host, port))
        feature_socket.listen(1)
        connection, address = feature_socket.accept()

        with connection:
            print("Connected to {}.".format(address))
            read_array_messages(connection, check_messages)


def read_array(socket):
    len_buf = read_bytes(socket, 4)
    # intrepret as unsigned 32-bit integer in little Endian encoding
    msg_len = struct.unpack("<I", len_buf)[0]
    msg_buf = read_bytes(socket, msg_len)
    return numpy.frombuffer(msg_buf, numpy.dtype("float32").newbyteorder("<"))


def read_bytes(socket, n_bytes_to_read):
    """Read exactly n bytes from the socket.
    """
    buffer = b""
    while n_bytes_to_read > 0:
        data = socket.recv(n_bytes_to_read)
        if data == b"":
            raise ZeroBytesRead(
                "Expected to read some bytes of the {} bytes left to read.".format(
                    n_bytes_to_read
                )
            )
        buffer += data
        n_bytes_to_read -= len(data)
    return buffer


if __name__ == "__main__":
    main()
