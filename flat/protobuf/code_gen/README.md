# Some notes about installing and using protobuf

For general protobuf setup I downloaded the 3.10.0 release and then build and installed it.

## Go

* Setup

`https://github.com/gogo/protobuf`

* Code Generation

`benchmark_feature_transfer/flat/protobuf/code_gen$ protoc -I=. -I=$GOPATH/src/ -I=$GOPATH/src/github.com/gogo/protobuf/protobuf --gogofaster_out=../go_sender/src/ features.proto`

## Python

* Setup

`pip3 install --user protobuf`

* Python

`protoc features.proto --python_out=../py_receiver/`
