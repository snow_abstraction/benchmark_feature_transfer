/*
Copyright 2019 Douglas Wayne Potter

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// This mostly C code and not C++. I was orginally C but then I wanted
// to use the C++ library "double-conversion" and I found it easiest
// to use C++, use a few convient C++ features and make the
// minimal changes to silence the compiler warnings.

#include <arpa/inet.h>
#include <assert.h>
#include <double-conversion/utils.h>
#include <float.h>
#include <math.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include "double-conversion/double-to-string.h"
#include "double-conversion/utils.h"

static int was_interrupted = 0;

void s_signal_handler(int signal_value) { was_interrupted = 1; }

// handle ctrl-c and kill
void setup_singal_handler(void) {
  struct sigaction action;
  action.sa_handler = s_signal_handler;
  action.sa_flags = 0;
  sigemptyset(&action.sa_mask);
  sigaction(SIGINT, &action, NULL);
  sigaction(SIGTERM, &action, NULL);
}

uint32_t create_json(int n_floats, const int upper_bound_message_length, char *buf) {
  auto & converter =
    double_conversion::DoubleToStringConverter::EcmaScriptConverter();
  auto string_builder = double_conversion::StringBuilder(buf, upper_bound_message_length);

  string_builder.AddCharacter('[');
  for (int i = 0; i < n_floats; ++i) {
    if (i != 0) {
      string_builder.AddCharacter(',');
    }
    converter.ToShortestSingle((float)sin(i), &string_builder);
  }
  string_builder.AddCharacter(']');

  return string_builder.position();
}

int main(int argc, char *argv[]) {
  setup_singal_handler();

  if (argc != 5) {
    fprintf(stderr, "Usage: send_features <number_of_samples> "
                    "<number_of_features> <host> <port>\n");
    exit(EXIT_FAILURE);
  }

  const int n_samples = atoi(argv[1]);
  if (n_samples < 1) {
    fprintf(stderr, "<number_of_samples> must be greater than zero.\n");
    exit(EXIT_FAILURE);
  }

  const int n_features = atoi(argv[2]);
  if (n_features < 1) {
    fprintf(stderr, "<number_of_features> must be greater than zero.\n");
    exit(EXIT_FAILURE);
  }

  printf("connecting to %s %s\n", argv[3], argv[4]);

  const int port = atoi(argv[4]);

  int socket_descriptor = socket(AF_INET, SOCK_STREAM, 0);
  if (socket_descriptor < 0) {
    fprintf(stderr, "Failed to create socket endpoint.");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_in servaddr;
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(port);

  if (inet_pton(AF_INET, argv[3], &(servaddr.sin_addr)) != 1) {
    fprintf(stderr, "Failed parse network address.");
    exit(EXIT_FAILURE);
  }

  if (connect(socket_descriptor, (struct sockaddr *)&servaddr,
              sizeof(servaddr)) != 0) {
    fprintf(stderr, "Failed to connect.");
    exit(EXIT_FAILURE);
  }

  int n_floats = n_samples * n_features;
  assert(4 * sizeof(uint8_t) == sizeof(float));
  const uint32_t upper_bound_float_reps_size =
      20 + 1; // characters/bytes per float plus comma;
  const uint32_t upper_bound_message_length =
      (1 + // null-terminator
       2 + // [ and ]
       n_floats * upper_bound_float_reps_size);

  char *data = new char[upper_bound_message_length];

  printf("Sending features (ctrl-c to quit).\n");

  long int n_sent = 0;
  clock_t start = time(0);

  while (1) {
    if (was_interrupted) {
      printf("Interrupted, shutting down...\n");
      break;
    }

    // This creation to done again and again to make this more like
    // the use case where the data needs to be copied in.
    const uint32_t message_length =
        create_json(n_floats, upper_bound_message_length, data);

    // assume that errors are interruptions, like the
    // socket being closed
    int error = write(socket_descriptor, &message_length, 4);
    if (error > -1) {
      write(socket_descriptor, data, message_length);
    } else {
      was_interrupted = 1;
    }

    ++n_sent;
  }

  clock_t end = time(0);

  delete[] data;

  double duration = difftime(end, start) * 1000.0;

  double sent_per_ms = ((double)n_sent) / duration;
  printf("number sent per ms: %f\n", sent_per_ms);

  close(socket_descriptor);

  return 0;
}
