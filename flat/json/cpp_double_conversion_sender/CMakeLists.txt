cmake_minimum_required(VERSION 3.13)

project(benchmark_feature_transformation)

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Debug)
endif()

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
  add_compile_options(-g -O0)
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/debug_bin)
elseif(CMAKE_BUILD_TYPE STREQUAL "Release")
  add_compile_options(-O3 -mavx -DNDEBUG)
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/release_bin)
elseif(CMAKE_BUILD_TYPE STREQUAL "Sanitize")
  add_compile_options(-g -O1 -fno-omit-frame-pointer -fsanitize=address)
  add_link_options(-fsanitize=address)
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/sanitize_bin)
else()
  message(FATAL_ERROR "Unknown type of build specified by CMAKE_BUILD_TYPE.")
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  add_compile_options(-ferror-limit=1)
endif()

add_compile_options(-Wall -Woverloaded-virtual -Wpedantic -Werror)

set(CMAKE_CXX_STANDARD 17)
set(CXX_STANDARD_REQUIRED YES)

find_package(double-conversion REQUIRED)

add_executable(sender
  src/main.cpp)

target_link_libraries(sender
  PRIVATE
      double-conversion
)
