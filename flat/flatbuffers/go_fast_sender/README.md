Project structure a la https://golang.org/doc/code.html#Organization

# set paths for go
export GOPATH=$THIS_PROJECT/benchmark_feature_transfer/flat/flatbuffers/go_fast_sender
export PATH=$PATH:$(go env GOPATH)/bin

# download flatbuffers package for go
# TODO: it seems that I should get a specific version
cd benchmark_feature_transfer/flat/flatbuffers/go_fast_sender
go get -u github.com/google/flatbuffers/go

# build and install sender
cd benchmark_feature_transfer/flat/flatbuffers/go_sender
go install sender

# run
sender

# generate flatbuffers code
cd benchmark_feature_transfer/flat/flatbuffers/code_gen
flatc --go features.fbs 
cp MyModel/Features.go ../go_fast_sender/src/sender/MyModel

# some Go tools
go vet
golint
