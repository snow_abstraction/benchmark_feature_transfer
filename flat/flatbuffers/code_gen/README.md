# Usage for creating: features_generated.h

Generate schema file `features.fbs`:
```
./flatbuffers_schema > features.fbs
```

From `features.fbs` generate C++:
```
flatc --cpp features.fbs

```
From `features.fbs` generate Python:
```
flatc --python features.fbs
cp -r MyModel ../py_reciever/
```




