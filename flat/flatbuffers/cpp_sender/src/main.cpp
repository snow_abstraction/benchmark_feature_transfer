/*
Copyright 2019 Douglas Wayne Potter

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include "../../code_gen/features_generated.h"

#include "flatbuffers/flatbuffers.h"

#include <boost/asio.hpp>

#include <csignal>

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <limits>
#include <string>
#include <chrono>
#include <ratio>
#include <memory>
#include <cstddef>
#include <cmath>

static bool was_interrupted = 0;

void s_signal_handler (int signal_value)
{
    was_interrupted = 1;
}

// handle ctrl-c and kill
void setup_singal_handler(void)
{
    struct sigaction action;
    action.sa_handler = s_signal_handler;
    action.sa_flags = 0;
    sigemptyset (&action.sa_mask);
    sigaction (SIGINT, &action, NULL);
    sigaction (SIGTERM, &action, NULL);
}

template <typename T>
std::vector<T> generate_sin_vector(
    const size_t size) {

  std::vector<T> rv;

  rv.reserve(size);
  for (size_t i = 0; i < size; ++i) {
    rv.emplace_back(std::sin(i));
  }
  return rv;
}


class SinFeaturesGeneratorGenerateOnce {
  const std::vector<float> samples;
public:
  explicit SinFeaturesGeneratorGenerateOnce(const int _n_samples,
                                            const int _n_features)
    :
    samples(generate_sin_vector<float>(_n_samples * _n_features)) {}


  void generate_samples(flatbuffers::FlatBufferBuilder &builder) {
    builder.Clear();
    auto data = builder.CreateVector(samples);
    auto features = MyModel::CreateFeatures(builder, data);
    builder.Finish(features);
  }
};

int main(int argc, char *argv[]) {
  using boost::asio::ip::tcp;

  setup_singal_handler();

  try {
    if (argc != 5) {
      std::cerr << "Usage: send_features"
          " <number_of_samples> <number_of_features>"
          " <host> <port>\n";
      return 1;
    }

    const int n_samples = std::stoi(std::string(argv[1]));
    if (n_samples < 1) {
      std::cerr << "<number_of_samples> must be greater than zero.\n";
      return 1;
    }

    const int n_features = std::stoi(std::string(argv[2]));
    if (n_features < 1) {
      std::cerr << "<number_of_features> must be greater than zero.\n";
      return 1;
    }


    boost::asio::io_service io_service;
    tcp::socket s(io_service);
    tcp::resolver resolver(io_service);

    std::cout << "connecting to "
              << argv[3]
              << " "
              << argv[4]
              << '\n';
    boost::asio::connect(s, resolver.resolve({argv[3], argv[4]}));

    SinFeaturesGeneratorGenerateOnce features_generator(
                                                        n_samples, n_features);
    flatbuffers::FlatBufferBuilder builder(1024);

    std::cout << "Sending random features (ctrl-c to quit).\n";

    long int n_sent = 0;
    auto start_time = std::chrono::high_resolution_clock::now();

    while (true) {
      if (was_interrupted) {
        std::cout << "Interrupted, shutting down..." << std::endl;
        break;
      }
      features_generator.generate_samples(builder);
      std::uint32_t message_length = static_cast<std::uint32_t>(builder.GetSize());
      boost::asio::write(
          s, boost::asio::buffer(
              reinterpret_cast<uint8_t*>(&message_length),
              4));
      boost::asio::write(
          s, boost::asio::buffer(builder.GetBufferPointer(), builder.GetSize()));
      ++n_sent;

    }

    auto end_time = std::chrono::high_resolution_clock::now();
    using double_milliseconds = std::chrono::duration<double, std::milli>;

    auto duration = std::chrono::duration_cast<double_milliseconds>
        (end_time - start_time).count();

    double sent_per_ms = static_cast<double>(n_sent) / duration;
    std::cout << "number sent per ms: " << sent_per_ms << '\n';

  } catch (std::exception &e) {
    std::cerr << "Exception: " << e.what() << "\n";
  }

  return 0;
}
