# benchmark\_feature\_transfer
This is a starter project (hack) for testing the performance and ease of getting an array of float32 into a Python `numpy.ndarray` over the network on Linux. The idea is test how well things work out of the box while keeping the serialization-deserialization simple. This project is at a lower-level then using a web framework like [flask](https://palletsprojects.com/p/flask/). The use-case is sending features to a Python [scikit-learn](https://scikit-learn.org/stable/) machine learning model in situation when the engineering cost of lower latency is worth the work and we cannot easily avoid Python.

## Transfer technologies tried:
1. raw - just pack the bytes myself
2. [flatbuffers](https://google.github.io/flatbuffers/)
3. [protocol buffers aka protobufs](https://developers.google.com/protocol-buffers)
4. JSON

## Some programming languages used:
1. C
2. C++
3. Go
4. Python

## Discussion:
1. For raw and and flatbuffers we could check that array sent was exactly the array received. This is not the case for JSON where we can only verify the numbers in the array are close. This could be fixed for JSON but not without more latency and code complexity.
2. flatbuffers has the advantage that we do not have to specify the data type in multiple places and thus avoid having to keep definitions in sync. It provides nice API with methods like `message.DataAsNumpy()`.
3. For me, the an important advantage of a serialization library is that message format is fixed and documented programmatically. 
4. The C++ flatbuffers sender is unnecessarily complex when compared to the raw and JSON senders since it began as a more ambitious project. It uses [boost Asio](https://www.boost.org/doc/libs/1_71_0/doc/html/boost_asio.html) but does not take advantage of Asio's support of asynchronous network programming.
5. The name flatbuffers is confusing since it supports sending nested data types.
6. A lot of things are missing in this project, for example proper Python packaging, robust command-line argument handling, tests, etc. It is a sort of a MVP; it is about quickly testing things.
7. The senders using JSON do not use any JSON library but install create JSON themselves. This seemed good enough for send one array but profiling revealed that almost all time was spent converting `float32` to strings using the `sprintf`. Also this meant the bottle-neck was in the C sender and not in the Python receiver. After I some research, I learned that the fasted JSON encoders use encoders/decoders like [double-conversion](https://github.com/google/double-conversion) so I used it in the sender for the result shown as "cpp_double_conversion_python_json". Then the bottle-neck was in the Python.
8. Protocol buffer performance was expected to be just slightly worse than flatbuffers but it was significantly worse. This was despite that I followed the standard performance advice: using the faster C++ extension (`PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=cpp`) and marking the data field `packed`. The bottleneck is converting to the `numpy.ndarray` and there does not seem to be a simple solution. There's an alternative and supposedly faster Python protobuf implementation called [pyrob](https://github.com/appnexus/pyrobuf) but it was not simply to use and is hasn't been tested with more recent Python versions. My earlier Protocol Buffer tests were as slow as the JSON using the [double-conversion](https://github.com/google/double-conversion). CPU profiling showed the one line taking 90% of the time: `array = numpy.array(msg.data)`. Replacing that line with `array = numpy.array(list(msg.data))` where `msg` is a Protocol Buffer data type, the performance improved 2X (code comments around this line contain more details). This is a good improvement but highly unintutive and still not fast at all consider how fast 100,000 contiguous `float32`s could be copied (packed repeated fields in a Protocol Buffer and `numpy.ararys` are both contiguous data structures). I also found in both variants that resulting `array` got filled `float64`s which is not intended but this behavior is [documented](https://developers.google.com/protocol-buffers/docs/proto3#scalar). The other transfer methods perservered `float32` as I intended. Having only used Protocol Buffers for communication between C++ programs professionally before, my takeway is that correct and performant usage in Python is more difficult than in C++ and not fully supported. In fact, [Python part of Protocol Buffers](https://github.com/protocolbuffers/protobuf/blob/master/python/README.md) states: 
   > The Python implementation of Protocol Buffers is not as mature as the C++ and Java implementations. It may be more buggy, and it is known to be pretty slow at this time. If you would like to help fix these issues, join the Protocol Buffers discussion list and let us know!

## Performance

That a raw byte encoding is 400X faster than JSON is expected. It was somewhat unexpected that using flatbuffers using C++ would have such low overhead when compared with the raw byte encoding using C—taking 1st and 2nd place with their places sometimes swapped when re-running the benchmarks but always around 150 μs per message.

![](images/micros_per_message.png)

Each message consists of 100000 32-bit floats sent over TCP between two processes on the same host. 

## Example Usage
To run the "benchmarks" and save results to `results.csv`:
```
./run_benchmarks.py 127.0.0.1 23425 1 results.csv
```
To analysis and plot the results:
open and run [analyze_resuts.ipynb](analyze_results.ipynb) in a [Jupyter notebook](https://jupyter.org/).

How to run the individual senders and receivers can be found in [run_benchmarks.py](run_benchmarks.py).

## Installation and building
You are own your but I can list what you need to be familiar with on your system in order to succeed:
1. running Python scripts and installing packages (`pip`) until they work,
2. installing C and C++ libraries and header files,
3. using the build tools `cmake`and `make`, and
4. using Go build tools.

## WIP
Other combinations of senders and recievers are in the works. They maybe in this repository without mention in this README.md. 

## License
benchmark\_feature\_transfer is under the Apache License, Version 2.0. See [LICENSE](LICENSE).

